# Wörterbuch
- sie sollen node.js installiren  [node js](https://nodejs.org/en/download/)
- sie sollen auch angular instalieren
 This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.3.
- sie können jetzt das project clonen
- run npm get uptate
- npm i bootstrap jquery --save

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
|Step | Linux Terminal Command  |
|---|---|
|1. Install git    | `sudo apt install git`  |
|2. Make sure that you've added a [SSH key](https://git.thm.de/profile/keys) | Refer to the link in the step column |
|3. Clone the remote repository  | `git clone git@git.thm.de:yanv02/woerterbuch.git` |
|4. sie sollen **"node.js"**  installiren  | [node js](https://nodejs.org/en/download/) |
|5. sie sollen auch angular instalieren |This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.3. |
|6. [bootstrap](https://getbootstrap.com/docs/4.6/getting-started/introduction/) installieren|npm i bootstrap jquery --save|

# api:
- wikipedia API
- Weather API

![](Screenshot2.png)
![](Screenshot3.png)
