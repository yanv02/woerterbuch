import {Component, OnInit} from '@angular/core';
import {WikiService} from './service/wiki/wiki.service';
import {NgForm} from "@angular/forms";
import {ajax} from "rxjs/ajax";
import {HttpClient} from "@angular/common/http";
import {environment} from "../environments/environment";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  pages :any;
  data: any;
  antwort: any;
  wikititle: string = '';
  wikiWordCount: string = '';
  wikiSnippet: string = '';


  title = 'dico app is running!';
  term= "yann";
  wort= 'space' ;
  imgmeteo:any  ;

  temperaturejson: any;
  descriptiontmp: any
  targetLanguage:String= 'de';

  constructor() {
  }
  ngOnInit(){
    this.getWeatherData();

  }
  wikiapi(wort:string){
    var url = "https://en.wikipedia.org/w/api.php";
    this.wikititle ='Title';
    this.wikiWordCount = 'Word count';
    this.wikiSnippet ='Snippet';


    var params = {
      action: "query",
      list: "search",
      srsearch: wort,
      format: "json"
    };

    url = url + "?origin=*";
    Object.keys(params).forEach(function(key){ // @ts-ignore
      url += "&" + key + "=" + params[key];});

    fetch(url)
        .then(function(response){return response.json();})
        .then((response) => {
          if (response.query.search[0].title === "Nelson Mandela"){
            console.log("Your search page 'Nelson Mandela' exists on English Wikipedia" );
          }
         this.pages =response;
        })
        .catch(function(error){console.log(error);});
  }

  getWeatherData(){
    fetch('https://api.openweathermap.org/data/2.5/weather?appid=07b01d0ec01523aba4404881c8b379fd&q=gießen&units=metric')
        .then(response=>response.json())
        .then(antwort=>{this.setjson(antwort);})
  }


  setjson(antwort:any){
    this.temperaturejson=antwort ;
    let img =this.temperaturejson.weather[0].icon;
    this.imgmeteo = "http://openweathermap.org/img/wn/" + img + "@2x.png";
    console.log(this.temperaturejson);

  }


}
