import { TestBed } from '@angular/core/testing';

import { GoogleTranlateService } from './google-tranlate.service';

describe('GoogleTranlateService', () => {
  let service: GoogleTranlateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GoogleTranlateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
